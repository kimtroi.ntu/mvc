<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 3/30/2019
 * Time: 9:22 PM
 */

class FT_Helper_Loader
{
    /**
     * Load helper
     *
     * @param   string
     * @desc    hàm load helper, tham số truyền vào là tên của helper
     */
    public function load($helper)
    {
        $helper = ucfirst($helper) . '_Helper';
        if (!file_exists(PATH_APPLICATION . '/helper/' . $helper . '.php')){
            die ('Không tìm thấy helper');
        }
        require_once(PATH_SYSTEM . '/helper/' . $helper . '.php');
    }
}