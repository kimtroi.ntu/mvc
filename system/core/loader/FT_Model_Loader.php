<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 3/30/2019
 * Time: 10:33 PM
 */

class FT_Model_Loader
{
    public function load($model)
    {
        $model = ucfirst($model) . '_Model';
        if (!file_exists(PATH_APPLICATION . '/model/' . $model . '.php')){
            die ('Không tìm thấy model');
        }
        require_once(PATH_APPLICATION . '/model/' . $model . '.php');
    }
}