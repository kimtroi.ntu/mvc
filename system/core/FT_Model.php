<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 3/30/2019
 * Time: 8:22 PM
 */

class FT_Model
{
    protected $conn = NULL;

    public function __construct($is_model = true)
    {
        if (!$this->conn) {
            $this->conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME)
            or die ('Không thể kết nối CSDL');
            mysqli_set_charset($this->conn, 'UTF-8');
        }
    }

    // Hàm ngắt kết nối
    public function db_close(){
        if ($this->conn){
            mysqli_close($this->conn);
        }
    }

    // Hàm lấy danh sách, kết quả trả về danh sách các record trong một mảng
    public function db_get_list($sql){
        $data  = array();
        $result = mysqli_query($this->conn, $sql);
        while ($row = mysqli_fetch_assoc($result)){
            $data[] = $row;
        }
        return $data;
    }

    // Hàm lấy chi tiết, dùng select theo ID vì nó trả về 1 record
    function db_get_row($sql){
        $result = mysqli_query($this->conn, $sql);
        $row = array();
        if (mysqli_num_rows($result) > 0){
            $row = mysqli_fetch_assoc($result);
        }
        return $row;
    }

    // Hàm thực thi câu truy  vấn insert, update, delete
    function db_execute($sql){
        return mysqli_query($this->conn, $sql);
    }
}