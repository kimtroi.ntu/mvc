<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 3/30/2019
 * Time: 11:09 PM
 */

class User_Controller extends Base_Controller
{
    public function indexAction()
    {
        $this->model->load('user');
        $model = new User_Model();
        $this->load_header();
        $data['user_list'] = $model->user_list();
        $this->view->load('user', $data);
        $this->load_footer();
        $model->db_close();
    }

    public function detailAction()
    {
        $this->model->load('user');
        $model = new User_Model();

        $this->load_header();

        if ( isset($_GET['id']) ) {
            $data['user_detail'] = $model->user_by_id($_GET['id']);
            $view = $data['user_detail'] ? 'user_detail' : 'user_not_found';
            $this->view->load($view, $data);
        } else {
            $this->view->load('user_not_found');
        };

        $this->load_footer();
        $model->db_close();
    }

    public function insertAction()
    {
        $this->model->load('user');
        $model = new User_Model();

        $this->load_header();

        $error = array();
        // Lấy danh sách dữ liệu từ form
        $data_post = array(
            'username'  => isset($_POST['username']) ? $_POST['username'] : '',
            'password'  => isset($_POST['password']) ? md5($_POST['password']) : '',
            're-password'  => isset($_POST['re-password']) ? md5($_POST['re-password']) : '',
            'email'     => isset($_POST['email']) ? $_POST['email'] : '',
            'fullname'  => isset($_POST['fullname']) ? $_POST['fullname'] : '',
            'level'     => isset($_POST['level']) ? $_POST['level'] : '',
        );

        // Nếu người dùng submit form
        if (isset($_POST['request_name']) && $_POST['request_name'] == 'add_user') {

            // Thực hiện validate
            $error = $model->user_validate($data_post);

            // Nếu validate không có lỗi
            if (!$error) {
                // Xóa key re-password ra khoi $data
                unset($data_post['re-password']);
                $model->user_insert($data_post);
            }
        }

        $data['data_post'] = $data_post;
        $data['error'] = $error;

        $this->view->load('user_insert', $data);

        $this->load_footer();
        $model->db_close();
    }
}