<?php if ( ! defined('PATH_SYSTEM')) die ('Bad requested!');

class View_Controller extends Base_Controller
{
    public function indexAction()
    {
        $data = array(
            'title' => 'Chào mừng các bạn đến với freetuts.net'
        );

        // Load view
        $this->load_header();
        $this->view->load('view', $data);
        $this->load_footer();
    }
}