<?php if ( ! defined('PATH_SYSTEM')) die ('Bad requested!');

class Product_Controller extends Base_Controller
{
    public function indexAction()
    {
        $this->load_header();
        $this->view->load('product');
        $this->load_footer();
    }
}