<h2>Danh sách user</h2>

<?php foreach ($user_list as $user) : ?>
<dl>
    <dt>User name:</dt>
    <dd><?= $user['username'] ?></dd>
    <dt>Email:</dt>
    <dd><?= $user['email'] ?></dd>
    <dt>Full name:</dt>
    <dd><?= $user['fullname'] ?></dd>
    <dt>Level:</dt>
    <dd><?= $user['level'] ?></dd>
</dl>
<?php endforeach; ?>
