<h2>Thêm thành viên</h2>

<div class="controls">
    <a class="button" onclick="jQuery('#main-form').submit()" href="#">Lưu</a>
    <a class="button" href="http://mvc.me/admin.php?c=user">Trở về</a>
</div>

<form id="main-form" method="post" action="http://mvc.me/admin.php?c=user&a=insert">
    <input type="hidden" name="request_name" value="add_user"/>
    <table cellspacing="0" cellpadding="0" class="form">
        <tr>
            <td width="200px">Tên đăng nhập</td>
            <td>
                <input type="text" name="username" value="<?php echo $data_post['username']; ?>" />
                <?= isset($error['username']) ? $error['username'] : ''; ?>
            </td>
        </tr>
        <tr>
            <td>Mật khẩu</td>
            <td>
                <input type="password" name="password" value="<?php echo $data_post['password']; ?>" />
                <?= isset($error['password']) ? $error['password'] : ''; ?>
            </td>
        </tr>
        <tr>
            <td>Nhập lại mật khẩu</td>
            <td>
                <input type="password" name="re-password" value="<?php echo $data_post['re-password']; ?>" />
                <?= isset($error['re-password']) ? $error['re-password'] : ''; ?>
            </td>
        </tr>
        <tr>
            <td>Email</td>
            <td>
                <input type="text" name="email" value="<?php echo $data_post['email']; ?>" class="long" />
                <?= isset($error['email']) ? $error['email'] : ''; ?>
            </td>
        </tr>
        <tr>
            <td>Fullname</td>
            <td>
                <input type="text" name="fullname" value="<?php echo $data_post['fullname']; ?>" class="long" />
                <?= isset($error['fullname']) ? $error['fullname'] : ''; ?>
            </td>
        </tr>
        <tr>
            <td>Level</td>
            <td>
                <select name="level">
                    <option value="">-- Chọn Level --</option>
                    <option value="1" <?php echo ($data_post['level'] == 1) ? 'selected' : ''; ?>>Admin</option>
                    <option value="2" <?php echo ($data_post['level'] == 2) ? 'selected' : ''; ?>>Member</option>
                </select>
                <?= isset($error['level']) ? $error['level'] : ''; ?>
            </td>
        </tr>
    </table>
</form>