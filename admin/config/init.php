<?php
/**
 * Created by PhpStorm.
 * User: ADMIN
 * Date: 3/30/2019
 * Time: 8:25 PM
 */

return array(
    'default_controller'    => 'index', // controller mặc định
    'default_action'        => 'index', // action mặc định
    '404_controller'        => 'error', // controller lỗi 404
    '404_action'            => 'index'  // action lỗi 404
);